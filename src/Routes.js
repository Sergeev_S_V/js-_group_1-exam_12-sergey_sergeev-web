import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddNewPhoto from "./components/AddNewPhoto/AddNewPhoto";
import PhotosList from "./containers/PhotosList/PhotosList";
import UserGallery from "./containers/UserGallery/UserGallery";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login" />
);

const Routes = ({user}) => (
  <Switch>
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <ProtectedRoute
      isAllowed={user}
      path="/add_new_photo"
      exact
      render={() => <AddNewPhoto user={user}/>}
    />
    <Route path="/photos/user/:id" exact component={UserGallery}/>
    <Route path="/" exact component={PhotosList}/>
    <Route path="/photos" exact component={PhotosList}/>
  </Switch>
);

export default Routes;