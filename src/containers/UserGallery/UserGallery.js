import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Image, Modal, PageHeader, Panel} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

import {deletePhoto, fetchUserPhotos, hideFullImage, showFullImage} from "../../store/actions/photos";
import config from "../../config";

const pathImage = config.apiUrl + '/uploads/';

class UserGallery extends Component {

  state = {
    image: ''
  };

  componentDidMount() {
    this.props.onFetchUserPhotos(this.props.match.params.id);
  };

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this.props.onFetchUserPhotos(this.props.match.params.id);
    }
  };

  showFullImageHandler = image => {
    this.setState({image});

    this.props.onShowFullImage();
  };

  renderUserPhotos = photos => {
    return photos.map(photo => (
      <Col md={3} key={photo._id}>
        <Panel>
          <Panel.Body>
            <Image src={pathImage + photo.image}
                   thumbnail
                   onClick={() => this.showFullImageHandler(photo.image)}
            />
            <Panel.Footer>
              <h6>{photo.title}</h6>
              {this.props.user && this.props.user._id === this.props.userOfGallery._id &&
                <Button onClick={() => this.props.onDeletePhoto(photo._id)}>
                  Delete
                </Button>
              }
            </Panel.Footer>
          </Panel.Body>
        </Panel>
      </Col>
    ));
  };

  render() {
    let modal;
    if (this.props.showFullImage) {
      modal = (
        <Modal
          show={this.props.showFullImage}
          onHide={this.props.onHideFullImage}
        >
          <Modal.Body>
            <Image src={pathImage + this.state.image}
                   thumbnail
                   style={{width: '100%'}}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.onHideFullImage}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )
    }

    return(
      <Fragment>
        {modal}
        <Col md={10}>
          <PageHeader>{this.props.userOfGallery.username} gallery</PageHeader>
        </Col>
        {this.props.user && this.props.user._id === this.props.userOfGallery._id &&
          <Col md={2}>
            <LinkContainer to='/add_new_photo'>
              <Button>Add new photo</Button>
            </LinkContainer>
          </Col>
        }
        <Col md={12}>
          {this.props.photos.length > 0
            && this.renderUserPhotos(this.props.photos)
          }
        </Col>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  photos: state.photos.userPhotos,
  userOfGallery: state.photos.userOfGallery,
  showFullImage: state.photos.showFullImage
});

const mapDispatchToProps = dispatch => ({
  onFetchUserPhotos: userId => dispatch(fetchUserPhotos(userId)),
  onDeletePhoto: photoId => dispatch(deletePhoto(photoId)),
  onShowFullImage: () => dispatch(showFullImage()),
  onHideFullImage: () => dispatch(hideFullImage())
});

export default connect(mapStateToProps, mapDispatchToProps)(UserGallery);