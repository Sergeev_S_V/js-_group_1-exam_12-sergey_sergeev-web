import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchPhotos, hideFullImage, showFullImage} from "../../store/actions/photos";
import {Button, Col, Image, Modal, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import config from "../../config";

const pathImage = config.apiUrl + '/uploads/';

class PhotosList extends Component {

  state = {
    image: ''
  };

  componentDidMount() {
    this.props.onFetchPhotos();
  };

  showFullImageHandler = image => {
    this.setState({image});

    this.props.onShowFullImage();
  };

  renderPhotos = photos => {
    return photos.map(photo => (
      <Col md={3} key={photo._id}>
        <Panel style={{width: '250px', height: '270px'}}>
          <Panel.Body>
            <Image src={pathImage + photo.image}
                   thumbnail
                   onClick={() => this.showFullImageHandler(photo.image)}
            />
            <Panel.Footer>
              <h6>{photo.title}</h6>
              <span>by:
                <Link to={`/photos/user/${photo.userId._id}`}>
                  {photo.userId.username}
                </Link>
              </span>
            </Panel.Footer>
          </Panel.Body>
        </Panel>
      </Col>
    ));
  };

  render() {
    let modal;
    if (this.props.showFullImage) {
      modal = (
        <Modal
          show={this.props.showFullImage}
          onHide={this.props.onHideFullImage}
        >
          <Modal.Body>
            <Image src={pathImage + this.state.image}
                   thumbnail
                   style={{width: '100%'}}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.onHideFullImage}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )
    }
    return(
      <Fragment>
        {modal}
        <Col md={12}>
          {this.props.photos.length > 0
            && this.renderPhotos(this.props.photos)
          }
        </Col>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  photos: state.photos.photos,
  showFullImage: state.photos.showFullImage
});

const mapDispatchToProps = dispatch => ({
  onFetchPhotos: () => dispatch(fetchPhotos()),
  onShowFullImage: () => dispatch(showFullImage()),
  onHideFullImage: () => dispatch(hideFullImage())
});

export default connect(mapStateToProps, mapDispatchToProps)(PhotosList);

