import React, {Component, Fragment} from 'react';
import {
  Button, Col, ControlLabel, Form, FormControl, FormGroup, PageHeader
} from "react-bootstrap";
import {connect} from "react-redux";
import {createPhoto} from "../../store/actions/photos";

class AddNewPhoto extends Component {

  state = {
    title: '',
    image: '',
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onCreatePhoto(formData);
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Add new photo</PageHeader>
        <Form horizontal onSubmit={this.submitFormHandler}>
          <FormGroup controlId="photoTitle">
            <Col componentClass={ControlLabel} sm={2}>
              Title
            </Col>
            <Col sm={10}>
              <FormControl
                type="text"
                required
                placeholder="Enter title"
                name="title"
                value={this.state.title}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="photoImage">
            <Col componentClass={ControlLabel} sm={2}>
              Image
            </Col>
            <Col sm={10}>
              <FormControl
                type="file"
                required
                name="image"
                onChange={this.fileChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">Add photo</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onCreatePhoto: photoData => dispatch(createPhoto(photoData))
});

export default connect(null, mapDispatchToProps)(AddNewPhoto);