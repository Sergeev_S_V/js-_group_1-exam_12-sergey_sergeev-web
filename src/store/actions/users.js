import axios from "../../axios-api";
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

import {
  FACEBOOK_LOGIN_FAILURE, FACEBOOK_LOGIN_SUCCESS, LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS,
} from "./actionTypes";

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      (response) => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', response.data.message);
      },
      error => {
        dispatch(registerUserFailure(error.response.data));
      }
    );
  };
};

const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const facebookLogin = data => async dispatch => {
  try {
    const response = await axios.post('/users/facebookLogin', data);
    dispatch(loginUserFacebookSuccess(response.data.user));
    dispatch(push('/'));
    NotificationManager.success('Logged or Registered in with Facebook!', response.data.message);
  } catch (error) {
    dispatch(loginUserFacebookFailure(error.response.data));
  }
};

const loginUserFacebookSuccess = user => {
  return {type: FACEBOOK_LOGIN_SUCCESS, user};
};

const loginUserFacebookFailure = error => {
  return {type: FACEBOOK_LOGIN_FAILURE, error};
};

export const loginUser = userData => dispatch => {
  return axios.post('/users/sessions', userData)
    .then(res => {
        dispatch(loginUserSuccess(res.data));
        dispatch(push('/'));
        NotificationManager.success('Success', 'Login success');
      },
      error => {
        const err = error.response ? error.response.data : {error: 'No internet connection'};
        dispatch(loginUserFailure(err));
      });
};

const loginUserSuccess = (user) => {
  return {type: LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = (error) => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const logoutUser = () => async dispatch => {
  const response = await axios.delete('/users/sessions');
  dispatch(push('/'));
  dispatch(logoutUserSuccess());
  NotificationManager.success('Success', response.data.message);
};

const logoutUserSuccess = () => ({
  type: LOGOUT_USER
});

export const logoutExpiredUser = () => dispatch => {
  dispatch(push('/'));
  dispatch(logoutUserSuccess());
  NotificationManager.error('Error', 'Your session has expired, please login again');
};