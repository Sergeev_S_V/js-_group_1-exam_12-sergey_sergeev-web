import axios from '../../axios-api';
import {push} from "react-router-redux";
import {
  CREATE_PHOTO_FAILURE, CREATE_PHOTO_SUCCESS, DELETE_PHOTO_FAILURE, DELETE_PHOTO_SUCCESS, FETCH_PHOTOS_FAILURE,
  FETCH_PHOTOS_SUCCESS,
  FETCH_USER_PHOTOS_FAILURE,
  FETCH_USER_PHOTOS_SUCCESS, HIDE_FULL_IMAGE,
  SHOW_FULL_IMAGE
} from "./actionTypes";

// create photo
export const createPhoto = photoData => async dispatch => {
  try {
    const response = await axios.post('/photos', photoData);
    dispatch(createPhotoSuccess(response.data));
    dispatch(push('/'));
  } catch (err) {
    dispatch(createPhotoFailure(err))
  }
};

const createPhotoSuccess = photo => ({
  type: CREATE_PHOTO_SUCCESS, photo
});

const createPhotoFailure = err => ({
  type: CREATE_PHOTO_FAILURE, err
});


// fetch photos
export const fetchPhotos = () => async dispatch => {
  try {
    const response = await axios.get('/photos');
    dispatch(fetchPhotosSuccess(response.data));
  } catch (err) {
    dispatch(fetchPhotosFailure(err));
  }
};

const fetchPhotosSuccess = photos => ({
  type: FETCH_PHOTOS_SUCCESS, photos
});

const fetchPhotosFailure = err => ({
  type: FETCH_PHOTOS_FAILURE, err
});

// showFullImage

export const showFullImage = () => ({
  type: SHOW_FULL_IMAGE
});

export const hideFullImage = () => ({
  type: HIDE_FULL_IMAGE
});

// fetchUserPhotos

export const fetchUserPhotos = userId => async dispatch => {
  try {
    const response = await axios.get(`/photos?userId=${userId}`);
    dispatch(fetchUserPhotosSuccess(response.data.photos, response.data.user));
  } catch (err) {
    dispatch(fetchUserPhotosFailure(err));
  }
};

const fetchUserPhotosSuccess = (userPhotos, user) => ({
  type: FETCH_USER_PHOTOS_SUCCESS, userPhotos, user
});

const fetchUserPhotosFailure = err => ({
  type: FETCH_USER_PHOTOS_FAILURE, err
});

// delete photo

export const deletePhoto = photoId => async dispatch => {
  try {
    const response = await axios.delete(`/photos?photoId=${photoId}`);
    dispatch(deletePhotoSuccess(response.data));
  } catch (err) {
    dispatch(deletePhotoFailure(err));
  }
};

const deletePhotoSuccess = userPhotos => ({
  type: DELETE_PHOTO_SUCCESS, userPhotos
});

const deletePhotoFailure = err => ({
  type: DELETE_PHOTO_FAILURE, err
});