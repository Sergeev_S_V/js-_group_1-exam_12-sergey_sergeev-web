import {
  CREATE_PHOTO_SUCCESS, DELETE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS, FETCH_USER_PHOTOS_SUCCESS, HIDE_FULL_IMAGE,
  SHOW_FULL_IMAGE
} from "../actions/actionTypes";

const initialState = {
  photos: [],
  userPhotos: [],
  userOfGallery: {},
  showFullImage: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PHOTO_SUCCESS:
      return {...state, photos: state.photos.concat(action.photo)};
    case FETCH_PHOTOS_SUCCESS:
      return {...state, photos: action.photos};
    case FETCH_USER_PHOTOS_SUCCESS:
      return {...state, userPhotos: action.userPhotos, userOfGallery: action.user};
    case DELETE_PHOTO_SUCCESS:
      return {...state, userPhotos: action.userPhotos};
    case SHOW_FULL_IMAGE:
      return {...state, showFullImage: true};
    case HIDE_FULL_IMAGE:
      return {...state, showFullImage: false};
    default:
      return state;
  }
};
export default reducer;